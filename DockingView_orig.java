/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   DockingView.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;
import org.jdesktop.docking.support.CompoundIcon;
import org.jdesktop.docking.support.RotatedIcon;
import org.jdesktop.docking.support.TextIcon;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DockingView_orig extends FrameView {
    
    
    public DockingView_orig (Application application) {
        super(application);
        
        browserTabs = new JTabbedPane();
        browserTabs.setName("browserTabs");
        analyticTabs = new JTabbedPane(JTabbedPane.BOTTOM);
        analyticTabs.setName("analyticTabs");
        statTabs = new JTabbedPane();
        statTabs.setName("statTabs");;
        editorTabs = new JTabbedPane();
        editorTabs.setName("editorTabs");
        
        panel = new JPanel(new BorderLayout());
        panel.setName("componentPanel");
        browserButtons = new JToolBar(JToolBar.VERTICAL);
        browserButtons.setName("browserButtons");
        browserButtons.setFloatable(false);
        analyticButtons = new JToolBar(JToolBar.HORIZONTAL);
        analyticButtons.setName("analyticButtons");
        analyticButtons.setFloatable(false);
        statsButtons = new JToolBar(JToolBar.VERTICAL);
        statsButtons.setName("statsButtons");
        statsButtons.setFloatable(false);
        
        panel.add(browserButtons, BorderLayout.WEST);
        panel.add(analyticButtons, BorderLayout.SOUTH);
        panel.add(statsButtons, BorderLayout.EAST);
        
        browserSplit = new JSplitPane();
        browserSplit.setName("browserSplit");
        editorSplit = new JSplitPane();
        editorSplit.setName("editorSplit");
        analyticSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        analyticSplit.setName("analyticSplit");
        
        browserSplit.setLeftComponent(browserTabs);
        editorSplit.setLeftComponent(editorTabs);
        editorSplit.setRightComponent(statTabs);
        analyticSplit.setRightComponent(analyticTabs);
    }
    
    public void addDock(DockingPanel dock) {
        String title = null;
        Icon icon = null;
        try {
            title = getContext()
                    .getResourceMap(dock.getClass())
                    .getString(dock.getName() + ".title");
        } catch (Exception e) {
            title = dock.getTitle();
        }
        try {
            icon = getContext()
                    .getResourceMap(getClass())
                    .getIcon(dock.getName() + ".icon");
        } catch (Exception e) {
            icon = dock.getIcon();
        }
        
        if (dock instanceof AnalyticDock) {
            analyticTabs.add(dock);
            configureTab(analyticTabs, dock);
        } else if (dock instanceof BrowserDock) {
            browserTabs.add(dock);
            configureTab(browserTabs, dock);
        } else if (dock instanceof EditorDock) {
            editorTabs.add(dock);
            configureTab(editorTabs, dock);
        } else if (dock instanceof StatsDock) {
            statTabs.add(dock);
            configureTab(statTabs, dock);
        }
        
        configure();
    }
    
    public void removeDock(DockingPanel dock) {
        if (dock instanceof BrowserDock) {
            removeTab(browserTabs, dock);
        } else if (dock instanceof AnalyticDock) {
            removeTab(analyticTabs, dock);
        } else if (dock instanceof EditorDock) {
            removeTab(editorTabs, dock);
        } else if (dock instanceof StatsDock) {
            removeTab(statTabs, dock);
        }
        
        configure();
    }
    
    public void iconifyTab(DockingPanel dock) {
        JButton btn = new JButton();
        Icon txtIcon = new TextIcon(btn, dock.getTitle());
        Icon cIcon = new CompoundIcon(CompoundIcon.Axis.X_AXIS, 2, 
                dock.getIcon(), txtIcon);
        Icon icon = null;
        
        if (dock instanceof AnalyticDock) {
            // Minimizes to the bottom...no rotation.
            icon = cIcon;
            btn.setText(dock.getTitle());
            btn.setIcon(dock.getIcon());
            btn.setPreferredSize(new Dimension(icon.getIconWidth(), btn.getHeight()));
            addButton(analyticButtons, btn);
        } else if (dock instanceof BrowserDock) {
            // Minimized to the left...rotate down.
            icon = new RotatedIcon(cIcon, RotatedIcon.Rotate.DOWN);
            btn.setIcon(icon);
            btn.setPreferredSize(new Dimension(btn.getWidth(), icon.getIconHeight()));
            addButton(browserButtons, btn);
        } else if (dock instanceof StatsDock) {
            icon = new RotatedIcon(cIcon);
            btn.setIcon(icon);
            btn.setPreferredSize(new Dimension(btn.getWidth(), icon.getIconHeight()));
            addButton(statsButtons, btn);
        } else {
            throw new IllegalArgumentException("Editors cannot be minimized");
        }
//        removeDock(dock);
        
        configure();
    }
    
    private void restoreTab(DockingPanel dock) {
        
    }
    
    private void addButton(JToolBar toolbar, JButton button) {
        toolbar.add(button);
        toolbar.setVisible(toolbar.getComponentCount() > 0);
    }
    
    private void removeButton(JToolBar toolbar, JButton button) {
        toolbar.remove(button);
        toolbar.setVisible(toolbar.getComponentCount() > 0);
    }
    
    private void removeTab(JTabbedPane pane, DockingPanel dock) {
        int idx = pane.indexOfComponent(dock);
        pane.removeTabAt(idx);
    } 
    
    private void configureTab(JTabbedPane pane, DockingPanel dock) {
        int idx = pane.indexOfComponent(dock);
        pane.setTabComponentAt(idx, new TabComponent(pane, this, dock));
    }
    
    private void configure() {
        // Start at the top: Editors. When we receive the component from 
        //+ configureEditorSplit(), it will be either the editorSplit, because
        //+ there are Stat panels open, or it will be the editorPane alone.
        JComponent editors = configureEditorSplit();
        
        // Next, we'll move up one level to the analyticSplit. If there are no
        //+ Analytics panels open, we will receive null in return. Otherwise,
        //+ we will receive the analyticSplit, when we will add the base
        //+ JComponent to the left component of that split.
        JComponent analytics = configureAnalyticSplit();
        if (analytics != null) {
            analyticSplit.setLeftComponent(editors);
        } else {
            // We will just set analytics equal to editors.
            analytics = editors;
        }
        
        // Now, we have worked our way all of the way up the daisy chain and 
        //+ need to see if we have any browser panels open.
        JComponent browsers = configureBrowserSplit();
        
        // If there are no browser panels open, browsers will be null. Otherwise,
        //+ browsers will be the browserSplit, so we will need to add analytics
        //+ as its right component.
        if (browsers != null) {
            browserSplit.setRightComponent(analytics);
        } else {
            // We will just set browsers equal to analytics.
            browsers = analytics;
        }
        
        // The last thing we need to do is add what we have configured to the
        //+ panel, in the center location.
        panel.remove(browsers);
        panel.add(browsers, BorderLayout.CENTER);
        setComponent(null);
        setComponent(panel);
        
        // Configure the minimization toolbars, setting their visibility based
        //+ solely upon whether they have buttons in them.
        browserButtons.setVisible(browserButtons.getComponentCount() > 0);
        analyticButtons.setVisible(analyticButtons.getComponentCount() > 0);
        statsButtons.setVisible(statsButtons.getComponentCount() > 0);
        
        // Update the UI and repaint as well.
        getComponent().updateUI();
        getComponent().repaint();
    }
    
    private JComponent configureBrowserSplit() {
//        JComponent c = configureAnalyticSplit();
        
        if (browserTabs.getTabCount() == 0) {
            return null;
        } else {
            browserSplit.setLeftComponent(browserTabs);
//            browserSplit.setRightComponent(c);
            return browserSplit;
        }
    }
    
    private JComponent configureAnalyticSplit() {
//        JComponent c = configureEditorSplit();
        
        if (analyticTabs.getTabCount() == 0) {
            return null;
        } else {
//            analyticSplit.setLeftComponent(c);
            analyticSplit.setRightComponent(analyticTabs);
            return analyticSplit;
        }
    }
    
    private JComponent configureEditorSplit() {
        if (statTabs.getTabCount() == 0) {
            return editorTabs;
        } else {
            editorSplit.setLeftComponent(editorTabs);
            editorSplit.setRightComponent(statTabs);
            return editorSplit;
        }
    }
    
    private final JPanel panel;
    
    private final JToolBar browserButtons;
    private final JToolBar analyticButtons;
    private final JToolBar statsButtons;
    
    private final JSplitPane browserSplit;
    private final JSplitPane analyticSplit;
    private final JSplitPane editorSplit;
    
    private final JTabbedPane browserTabs;
    private final JTabbedPane analyticTabs;
    private final JTabbedPane statTabs;
    private final JTabbedPane editorTabs;

}
