/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   DockingView.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 6, 2023
 *  Modified   :   Jan 6, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 6, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.docking;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.Application.ExitListener;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.StorageLocations;
import org.jdesktop.application.SysExits;
import org.jdesktop.docking.support.CompoundTextIcon;
import org.jdesktop.docking.support.ExternalFrame;
import org.jdesktop.docking.support.IconButton;
import org.jdesktop.docking.support.MaximizedTitleBar;
import org.jdesktop.docking.support.RotatedIcon;
import org.jdesktop.docking.support.TabComponent;
import org.jdesktop.docking.support.TextIcon;
import org.jdesktop.docking.trials.AnalyticDock;
import org.jdesktop.docking.trials.AnalyticPanel;
import org.jdesktop.docking.trials.BrowserDock;
import org.jdesktop.docking.trials.BrowserPanel;
import org.jdesktop.docking.trials.EditorDock;
import org.jdesktop.docking.trials.EditorPanel;
import org.jdesktop.docking.trials.StatsDock;
import org.jdesktop.docking.trials.StatsPanel;
import org.jdesktop.ui.swingx.MultiSplitLayout;
import org.jdesktop.ui.swingx.MultiSplitPane;
import org.jdesktop.utils.ColorUtils;
import org.jdesktop.utils.Level;
import org.jdesktop.utils.Logger;

/**
 * <a id="classSummary"></a>The {@code DockingView} class provides a subclass of
 * {@link org.jdesktop.application.FrameView FrameView} that provides the
 * functionality for docking windows that subclass
 * {@link org.jdesktop.docking.DockingPanel DockingPanel} into specific
 * locations determined by the implementing application.
 * <p>
 * The default implementation, achieved by calling the {@link #DockingView2()
 * DockingView(Application)} constructor, will provide a two-sided docking
 * layout, which can be visualized as:</p>
 * <pre>
 * +-----------------------+--------------------------------------------------+
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * |      LEFT SIDE        |                    RIGHT SIDE                    |
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * |                       |                                                  |
 * +-----------------------+--------------------------------------------------+
 * </pre>
 * <p>
 * In every respect, this is much the same as simply using a {@link
 * javax.swing.JSplitPane JSplitPane}, except that this layout (and any custom
 * layout that may be supplied by a subclass) can be defined by a string value
 * and is, by default, has its session state saved and restored, which would
 * need to be implemented for a {@code JSplitPane}.</p>
 * <p>
 * The string value that creates the default layout is defined as:</p>
 * <pre>
 * (ROW (LEAF name=left weight=0.3) (LEAF name=right weight=0.7))
 * </pre>
 * <p>
 * Subclasses may specify their own docking layout by supplying the definition
 * string to the secondary constructor null {@link #DockingView2(org.jdesktop.application.Application, java.lang.String) 
 * DockingView(Application, String)}. As an example of another definition, look
 * at this string:</p>
 * <pre>
 * (COLUMN (ROW weight=1.0 left (COLUMN middle.top middle middle.bottom) right) bottom)
 * </pre>
 * <p>
 * The layout as defined by this string would be:</p>
 * <pre>
 * +-----------------------+-----------------------------+--------------------+
 * |                       |                             |                    |
 * |                       |         Middle Top          |                    |
 * |                       |                             |                    |
 * +                       +-----------------------------+                    +
 * |                       |                             |                    |
 * |          Left         |           Middle            |        Right       |
 * |                       |                             |                    |
 * +                       +-----------------------------+                    +
 * |                       |                             |                    |
 * |                       |       Middle Bottom         |                    |
 * |                       |                             |                    |
 * +-----------------------+-----------------------------+--------------------+
 * |                                   Bottom                                 |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 * </pre>
 * <p>
 * So, as you can see, you do not need to use such words as "LEAF" or "SPLIT" in
 * your definition strings&hellip;you can also use "ROW" and "COLUMN", or use
 * these last two alone. However, by defining "LEAF"s in your definition string,
 * you can provide a little more control over their default sizing scheme.</p>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DockingView extends FrameView implements ExitListener {

    /**
     * {@code IconDirection} is an enumeration of constant values for which
     * direction the {@link org.jdesktop.docking.DockingPanel DockingPanels} are
     * to be iconified. {@code DockingView} only offers three options:
     * <ol><li>Left/West window border</li>
     * <li>Bottom/South window border</li>
     * <li>Right/East window border</li></ol>
     * These enumerated constants relate to each of those icon positions.
     */
    public enum IconDirection {
        EAST, SOUTH, WEST
    }

    /**
     * {@code DockState} is an enumeration of constant values that specify the
     * docking state <strong><em>to which</em></strong> the {@link
     * org.jdesktop.docking.DockingPanel DockingPanel} is moving. In other
     * words, if the {@code DockingPanel} is currently in its normal docked
     * state and the clicked on the iconify button, the state would be {@code
     * DockState.ICONIFYING}, as the {@code DockingPanel} is <em>moving toward
     * </em> its iconified state.
     */
    public enum DockState {
        EXTERNALIZING, FLOATING, ICONIFYING, INTERNALIZING, MAXIMIZING, NORMALIZING
    }

    /**
     * Constructs a new, default instance of {@code DockingView} for the default
     * running {@link org.jdesktop.application.Application Application}.
     * <p>
     * This constructor is really only present for use with {@link
     * org.jdesktop.api.Lookup Lookup} or the {@link java.util.ServiceLoader
     * ServiceLoader} mechanism.</p>
     */
    public DockingView() {
        this(Application.getInstance());
    }

    /**
     * Constructs a new instance of the {@code DockingView} window for the
     * specified {@link org.jdesktop.application.Application Application}.
     *
     * @param application the {@code Application} in which this window is being
     * used
     */
    public DockingView(Application application) {
        this(application, defaultLayoutDefinition);
    }

    /**
     * Constructs a new, customized instance of the {@code DockingView} based on
     * the specified {@code layoutDefinition}.
     *
     * @param application the {@code Application} in which this
     * {@code DockingView} is being used
     * @param layoutDefinition the definition of the docking layout to be used
     */
    public DockingView(Application application, String layoutDefinition) {
        super(application);
        logger = getContext().getAnonymousLogger();
        this.layoutDefinition = layoutDefinition;
        configPath = getContext()
                .getLocalStorage()
                .getConfigDirectory().toString();
        actionMap = getContext().getActionMap(DockingView.this);
        extFrame = new ExternalFrame(this);

        analytics = new ArrayList<>();
        browsers = new ArrayList<>();
        editors = new ArrayList<>();
        stats = new ArrayList<>();

        initComponents();
        
        initBrowserWidth = 200 + dockingPane.getDividerSize();
        initAnalyticHeight = 150 + dockingPane.getDividerSize();
    }

    /**
     * Adds a {@link org.jdesktop.docking.DockingPanel DockingPanel} into the
     * predefined docking locations.
     * <p>
     * The {@code DockingPanel} should provide some mechanism that will allow
     * the {@code DockingView} subclass to determine where the {@code dock}
     * should be placed. This may be implemented in various different ways, such
     * as using a {@link java.lang.Enum Enum} that can be supplied by a {@code
     * DockingPanel} subclass. Or, interfaces may be defined which can be
     * implemented by the {@code DockingPanel} subclass that simply mark the
     * panel as a certain type of dock that will be placed in a predefined
     * location.</p>
     *
     * @param dock the {@code DockingPanel} subclass to be docked
     * @throws IllegalArgumentException if {@code dock} is null
     */
    public void addDock(DockingPanel dock) {
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        if (dock instanceof BrowserDock) {
            browsers.add(dock);
            addBrowser(dock);
        } else if (dock instanceof AnalyticDock) {
            analytics.add(dock);
            addAnalytic(dock);
        } else if (dock instanceof EditorDock) {
            editors.add(dock);
            addEditor(dock);
        } else if (dock instanceof StatsDock) {
            stats.add(dock);
            addStat(dock);
        } else {
            throw new IllegalArgumentException("Unknown dock type");
        }
        getDockingPane().updateUI();
        getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
    }

    /**
     * Removes a {@link org.jdesktop.docking.DockingPanel DockingPanel} from the
     * predefined docking locations. The {@code DockingPanel} should provide
     * some mechanism that will allow the {@code DockingView} subclass to
     * determine where the {@code dock} should be placed. This may be
     * implemented in various different ways, such as using a
     * {@link java.lang.Enum Enum} that can be supplied by a {@code
     * DockingPanel} subclass. Or, interfaces may be defined which can be
     * implemented by the {@code DockingPanel} subclass that simply mark the
     * panel as a certain type of dock that will be placed in a predefined
     * location.</p>
     * <p>
     * If the specified {@code dock} is null, no exception is thrown and no
     * action is taken.</p>
     *
     * @param dock the {@code DockingPanel} to be removed (undocked)
     * @throws IllegalStateException if the {@code dock} was not previously
     * added to the docking layout
     */
    public void removeDock(DockingPanel dock) {
        if (dock == null) {
            return;
        }
        if (dock instanceof AnalyticDock) {
            removeAnalytic(dock);
        } else if (dock instanceof BrowserDock) {
            removeBrowser(dock);
        } else if (dock instanceof EditorDock) {
            removeEditor(dock);
        } else if (dock instanceof StatsDock) {
            removeStat(dock);
        } else {
            throw new IllegalArgumentException("Unknown dock type");
        }
        getDockingPane().updateUI();
        getDockingPane().getMultiSplitLayout().layoutContainer(getDockingPane());
        getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
    }

    /**
     * Iconifies a {@link org.jdesktop.docking.DockingPanel DockingPanel} to the
     * appropriate iconified location for the application.
     * <p>
     * By default, there are three iconifying locations provided by the {@code
     * DockingView}:</p>
     * <ol>
     * <li>WEST &mdash; a button bar on the left side of the window. This is the
     * location to which iconified docks from the left docking locations should
     * be placed. This location turns the icon buttons vertically, with the
     * "bottom" of the text toward the left window border.</li>
     * <li>EAST &mdash; a button bar on the right side of the window. This is
     * the location to which iconified docks from the right docking locations
     * should be placed. This location turns the icon buttons vertically, with
     * the "bottom" of the text toward the right window border.</li>
     * <li>SOUTH &mdash; a button bar on the bottom side of the window. This is
     * the location to which iconified docks from the bottom or central docking
     * locations should be placed. This location simply shows the icon buttons
     * with no orientation changes and the bottom of the text toward the bottom
     * window border.</li></ol>
     * <dl>
     * <dt>Implementation Notes</dt>
     * <dd>This method <em>must be</em> implemented by the subclass, but the
     * subclass is only required to call the {@link
     * #addIconButton(org.jdesktop.docking.DockingPanel,
     * org.jdesktop.docking.DockingView2.IconDirection) addIconButton} method
     * with the proper {@link org.jdesktop.docking.DockingView2.IconDirection
     * IconDirection} for the dock to be iconified to the proper button bar. The
     * {@code addIconButton} method will call the {@link
     * #removeDock(org.jdesktop.docking.DockingPanel) removeDock} method to make
     * sure the dock is not visible in two places at once.</dd></dl>
     *
     * @param dock the {@code DockingPanel} to be iconified
     * @throws IllegalArgumentException if {@code dock} is null
     * @throws IllegalStateException if {@code dock} is already iconified
     */
    public void iconifyDock(DockingPanel dock) {
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        if (dock instanceof BrowserDock) {
            addIconButton(dock, IconDirection.WEST);
            removeDock(dock);
        } else if (dock instanceof StatsDock) {
            addIconButton(dock, IconDirection.EAST);
            removeDock(dock);
        } else {
            addIconButton(dock, IconDirection.SOUTH);
            removeDock(dock);
        }
    }

    /**
     * Maximizes a {@link org.jdesktop.docking.DockingPanel DockingPanel} to
     * fill the entire client area of the window.
     * <p>
     * This method displays the {@code DockingPanel} with a title bar, which
     * includes the icon and title of the {@code DockingPanel}, as well as a
     * button to restore the dock to its original location. If the dock is
     * closable, a close button will also be included in the title bar. The same
     * is true for an externalize button, if the dock is externalizable.</p>
     * <dl>
     * <dt>Implementation Notes</dt>
     * <dd>This method <em>must be</em> implemented by the subclass in order to
     * make sure that the {@code DockingPanel} is removed from the docking. When
     * ready to complete the maximization of the dock, pass the
     * {@code DockingPanel} to the method {@link
     * #completeMaximization(org.jdesktop.docking.DockingPanel) completeMaximization}.
     * This method will attach a title bar to the dock and display it in such a
     * way as to cover the entire client area.</dd></dl>
     *
     * @param dock the {@code DockingPanel} to maximize
     * @throws IllegalArgumentException if {@code dock} is null
     * @throws IllegalStateException if {@code dock} is already maximized
     */
    public void maximizeDock(DockingPanel dock) {
        // TODO: Provide implementation in maximizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Externalizes a {@link org.jdesktop.docking.DockingPanel DockingPanel} to
     * its own window.
     * <p>
     * This method allows for the {@code DockingPanel} to be displayed within
     * its own external window from the {@code DockingView}. Though in its own
     * window, the dock will still be in a tab, as the externalized window is a
     * singleton and may contain other externalized docs as well.</p>
     * <dl>
     * <dt>Implementation Notes</dt>
     * <dd>This method <em>must be</em> implemented by the subclass in order to
     * make sure that the {@code DockingPanel} is removed from its normal
     * docking location. This is to prevent the dock from being in two places at
     * once, which could result in unforeseen issues.<br><br>
     * When the dock has been removed from its normal docking location, call the
     * method {@link #completeExternalization(org.jdesktop.docking.DockingPanel)
     * completeExternalization} to get the dock into the external window.</dd>
     * </dt>
     *
     * @param dock the {@code DockingPanel} to be externalized
     * @throws IllegalArgumentException if {@code dock} is null
     * @throws IllegalStateException if {@code dock} is already externalized
     */
    public void externalizeDock(DockingPanel dock) {
        // TODO: Provide implementation in externalizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Internalizes a {@link org.jdesktop.docking.DockingPanel DockingPanel}
     * that has been previously externalized.
     * <p>
     * In order to internalize the {@code DockingPanel}, simply call the method null     {@link #beginInternalization(org.jdesktop.docking.DockingPanel) 
     * beginInternalization}. This will make sure that the dock has been removed
     * from the external window (and close that window if it is now empty). Once
     * the dock is removed from the externalized window, it may be placed back
     * into its normal docking position.</p>
     * <p>
     * If {@code dock} is null or was not previously externalized, no exception
     * is thrown and no action is taken.</p>
     *
     * @param dock the {@code DockingPanel} to be internalized
     */
    public void internalizeDock(DockingPanel dock) {
        // TODO: Provide implementation in internalizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Restores a previously {@link #iconifyDock(org.jdesktop.docking.DockingPanel)
     * iconified} or {@link #maximizeDock(org.jdesktop.docking.DockingPanel)
     * maximized} dock.
     * <p>
     * If the dock was maximized, call the method null     {@link #beginNormalization(org.jdesktop.docking.DockingPanel) 
     * beginNormalization} to remove the dock from the maximized state and to
     * remove its title bar that was created.</p>
     * <p>
     * If {@code dock} is null, no exception is thrown and no action is taken.
     * </p>
     *
     * @param dock the {@code DockingPanel} to be restored to a normal state
     * @throws IllegalStateException if {@code dock} was not previously
     * iconified nor maximized
     */
    public void restoreDock(DockingPanel dock) {
        addDock(dock);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        super.propertyChange(evt);  // Let the View handle what it needs.

        MultiSplitLayout layout = getDockingPane().getMultiSplitLayout();
        MultiSplitLayout.Node node = layout.getNodeForName("editors");
        Rectangle bounds = node.getBounds();
        int dividerSize = dockingPane.getDividerSize();

        if (evt.getPropertyName() != null) {
            int adjustment;
            switch (evt.getPropertyName()) {
                case "componentAdded":
                    switch ((String) evt.getNewValue()) {
                        case "browsers":
                            adjustment = browserTabs.getMinimumSize().width
                                    + dividerSize;
                            if (browserTabs.getTabCount() == 0) {
//                                bounds.width += adjustment;
                                bounds.x = 0;
                                node.setBounds(bounds);
                                break;
                            } else {
//                                bounds.width -= adjustment;
                                bounds.x += adjustment;
                            }
                            break;
                        case "analytics":
                            adjustment = analyticTabs.getMinimumSize().height
                                    + dividerSize;
                            if (analyticTabs.getTabCount() == 0) {
                                bounds.height += adjustment;
                            } else {
                                bounds.height -= adjustment;
                            }
                    }
                    break;
                case "componentRemoved":
                    switch ((String) evt.getNewValue()) {
                        case "browsers":
                            adjustment = browserTabs.getMinimumSize().width
                                    + dividerSize;
                            if (browserTabs.getTabCount() == 0) {
//                                bounds.width += adjustment;
                                bounds.x = 0;
                                node.setBounds(bounds);
                                break;
                            } else {
//                                bounds.width -= adjustment;
                                bounds.x += adjustment;
                            }
                            break;
                        case "analytics":
                            adjustment = analyticTabs.getMinimumSize().height
                                    + dividerSize;
                            if (analyticTabs.getTabCount() == 0) {
                                bounds.height += adjustment;
                            } else {
                                bounds.height -= adjustment;
                            }
                    }
                    break;
                case "displayNode":
                    if (evt.getOldValue() != null 
                            && evt.getOldValue() instanceof String){
                        String name = (String) evt.getOldValue();
                        switch (name) {
                            case "browsers":
                                adjustment = browserTabs.getMinimumSize().width
                                        + dividerSize;
                                if (((Boolean) evt.getNewValue())) {
                                    // Node is showing:
                                    bounds.width -= adjustment;
                                    bounds.x += adjustment;
                                } else {
                                    bounds.width += adjustment;
                                    bounds.x -= adjustment;
                                }
                                break;
                            case "analytics":
                                adjustment = analyticTabs.getMinimumSize().height
                                        + dividerSize;
                                if (((Boolean) evt.getNewValue())) {
                                    // Node is showing:
                                    bounds.height -= adjustment;
                                } else {
                                    bounds.height += adjustment;
                                }
                                break;
                        }
                    }
            }
            
            layout.layoutByWeight(dockingPane);
            dockingPane.updateUI();
        }
    }

    /**
     * {@inheritDoc }
     * <p>
     * <strong><em>This implementation simply returns {@code true}, as the
     * {@code DockingView} does nothing to prevent exiting the
     * {@code Application}.</em></strong></p>
     *
     * @param event {@inheritDoc }
     */
    @Override
    public boolean canExit(EventObject event) {
        return true;    // We do nothing to prevent exiting.
    }

    /**
     * {@inheritDoc }
     * <p>
     * This implementation simply saves the {@code dockingPane}'s session state
     * so that it can be restored upon {@code Application} restart.</p>
     *
     * @param event {@inheritDoc }
     */
    @Override
    public void willExit(EventObject event) {
        XMLEncoder out = null;
        try {
            out = new XMLEncoder(
                    getContext()
                            .getLocalStorage()
                            .openOutputFile(
                                    StorageLocations.CONFIG_DIR,
                                    sessionFileName
                            )
            );
            modelRoot = dockingPane.getMultiSplitLayout().getModel();
            out.writeObject(modelRoot);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to store the docking session "
                    + "state to \"{0}{1}{2}\"", ex, configPath,
                    File.separator, sessionFileName);
        } finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }

    @Action
    public void restoreFromIcon(ActionEvent obj) {
        JButton btn = null;
        if (obj.getSource() instanceof JButton) {
            btn = (JButton) obj.getSource();
        }

        DockingPanel dock = null;
        IconDirection direction = null;
        if (obj.getSource() instanceof IconButton) {
            dock = ((IconButton) obj.getSource()).getDockingPanel();
            direction = ((IconButton) obj.getSource()).getIconDirection();
        }

        if (direction != null) {
            removeIconButton(dock, direction);
        }
        if (dock != null) {
            restoreDock(dock);
        }
    }

    /**
     * Adds a button to the appropriate button bar as a representation of the
     * iconified {@link org.jdesktop.docking.DockingPanel DockingPanel}.
     * <p>
     * This method takes the specified {@code DockingPanel} and creates a
     * special subclass of {@link javax.swing.JButton JButton} to represent the {@code
     * DockingPanel} on the appropriate button bar, as deteremined by the {@code
     * direction} specified. The representative button has an {@link
     * javax.swing.Action Action} associated with it that simply calls the {@link
     * #removeDock(org.jdesktop.docking.DockingPanel) removeDock()} method. Once
     * that call has returned, the {@code Action} removes the button from the
     * button bar. However, restoration of the {@code DockingPanel} to the
     * correct docking location is the responsibility of the {@code DockingView}
     * subclass.</p>
     *
     * @param dock the {@code DockingPanel} to become iconified
     * @param direction the direction to which the {@code DockingPanel} is to be
     * iconified &mdash; this determines on which button bar it is placed
     * @throws IllegalArgumentException if {@code dock} is null
     */
    protected void addIconButton(DockingPanel dock, IconDirection direction) {
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        IconButton button = new IconButton(dock, direction);
        button.setName(dock.getTitle().replace(" ", "_") + "_Button");
        button.setAction(actionMap.get("restoreFromIcon"));
        button.setText("");
        button.setHorizontalAlignment(SwingConstants.CENTER);
        button.setVerticalAlignment(SwingConstants.CENTER);

        Icon tmp = null;
        Icon txtIcon = new TextIcon(button, dock.getTitle());

        if (dock.getIcon() != null) {
            tmp = new CompoundTextIcon(dock.getIcon(), txtIcon);
        } else {
            tmp = txtIcon;
        }

        switch (direction) {
            case EAST:
                button.setIcon(new RotatedIcon(tmp, RotatedIcon.Rotate.UP));
                button.setVerticalAlignment(SwingConstants.BOTTOM);
                eastButtonBar.add(button);
                break;
            case SOUTH:
                button.setIcon(dock.getIcon());
                button.setText(dock.getTitle());

                southButtonBar.add(button);
                break;
            case WEST:
                button.setIcon(new RotatedIcon(tmp, RotatedIcon.Rotate.DOWN));
                button.setVerticalAlignment(SwingConstants.TOP);
                westButtonBar.add(button);
                break;
        }
    }

    /**
     * This method completes the maximization of the {@code DockingPanel}, once
     * the {@link #maximizeDock(org.jdesktop.docking.DockingPanel) maximizeDock}
     * method has taken care of removing the dock from its preferred docking
     * location.
     * <p>
     * This method creates a title bar for the dock and adds it, then displays
     * the dock in the entirety of the client area of the window. The only other
     * docks that will be visible are those that have been iconified to the
     * button bars.</p>
     * <dl>
     * <dt>Developer's Note</dt>
     * <dd>A good practice would be to iconify all open docks when one is set to
     * be maximized. By doing this, the user will still be able to get to the
     * other windows in fly-out ({@code DockState.FLOATING}) mode.</dd></dt>
     *
     * @param dock the {@code DockingPanel} to be maximized
     */
    protected void completeMaximization(DockingPanel dock) {
        dock.add(getTitleBar(dock, DockState.MAXIMIZING),
                BorderLayout.PAGE_START);
        setComponent(dock);
    }

    /**
     * Adds the {@code DockingPanel} to an externalized window. The result will
     * be the specified {@code dock} being a tab in the external window.
     * <p>
     * This method returns its success state of externalizing the dock. It is
     * advised to check this value and, if it is {@code false}, to put the
     * {@code DockingPanel} back into its preferred docking location.</p>
     *
     * @param dock the {@code DockingPanel} to externalize
     * @return {@code true} if the {@code dock} was successfully externalized;
     * {@code false} if externalization failed
     */
    protected boolean completeExternalization(DockingPanel dock) {
        return extFrame.addDock(dock);
    }

    /**
     * This method should be the first call from the {@link
     * #internalizeDock(org.jdesktop.docking.DockingPanel) internalizeDock}
     * method. First, this method gets hold of the externalization window and
     * obtains the specified {@code DockingPanel}. Then, that {@code dock} is
     * removed from the external window. If that is the last dock in the
     * external window, the window is closed, otherwise, it remains open for the
     * remaining docks.
     * <p>
     * This method returns its success state of internalizing the dock. It is
     * advised to check this value and, if it is {@code false}, to <em>not</em>
     * put the {@code DockingPanel} back into its preferred docking location.
     * </p>
     *
     * @param dock the {@code DockingPanel} to be internalized
     * @return {@code true} if the {@code dock} was successfully internalized;
     * {@code false} if internalization failed
     */
    protected boolean beginInternalization(DockingPanel dock) {
        return extFrame.removeDock(dock);
    }

    /**
     * This method should be the first call from {@link
     * #restoreDock(org.jdesktop.docking.DockingPanel) restoreDock} when dealing
     * with a maximized {@code DockingPanel}. First, this method brings down the
     * currently maximized {@code DockingPanel}. Then, the title bar is removed
     * from it. Once all of that is done, the docking pane is restored.
     *
     * @param dock the dock to begin normalizing
     * @throws IllegalStateException if the specified {@code dock} is not the
     * currently maximized dock or is not an instance of {@code DockingPanel}
     */
    protected void beginNormalization(DockingPanel dock) {
        Component c = getComponent();
        if ((!(c instanceof DockingPanel)) || !c.equals(dock)) {
            throw new IllegalStateException("The specified dock is not the "
                    + "currently maximized DockingPanel");
        }

        DockingPanel d = (DockingPanel) c;
        for (Component comp : d.getComponents()) {
            if (comp instanceof MaximizedTitleBar) {
                d.remove(comp);
                break;
            }
        }

        setComponent(dockingPane);
    }

//    protected JPanel getButtonBar(IconDirection direction) {
//        switch (direction) {
//            case EAST:
//                return eastButtonBar;
//            case SOUTH:
//                return southButtonBar;
//            case WEST:
//                return westButtonBar;
//            default:
//                throw new IllegalArgumentException("unknown IconDirection");
//        }
//    }
    /**
     * Retrieves the docking pane in use.
     *
     * @return the docking pane
     */
    protected MultiSplitPane getDockingPane() {
        return dockingPane;
    }

    /**
     * Convenience method to retrieve the {@link
     * org.jdesktop.ui.swingx.MultiSplitLayout docking layout}.
     *
     * @return the docking layout
     */
    protected MultiSplitLayout getDockingLayout() {
        return getDockingPane().getMultiSplitLayout();
    }

    /**
     * Convenience method to retrieve the root docking {@link
     * org.jdesktop.ui.swingx.MultiSplitLayout.Node Node} of the docking layout.
     *
     * @return the root node
     */
    protected MultiSplitLayout.Node getDockingModel() {
        return getDockingLayout().getModel();
    }

    private JPanel getTitleBar(DockingPanel dock, DockState state) {
        switch (state) {
            case FLOATING:
                // TODO: Create a title bar for floating (fly-out) docks.
                return null;
            case MAXIMIZING:
                return new MaximizedTitleBar(this, dock);
            default:
                throw new IllegalStateException("The DockState \"" + state.name()
                        + "\" is not a state that requires a title bar.");
        }
    }

    private void removeIconButton(DockingPanel dock, IconDirection direction) {
        if (dock == null) {
            return;
        }

        switch (direction) {
            case EAST:
                boolean removedEast = false;
                for (Component c : eastButtonBar.getComponents()) {
                    if (c instanceof IconButton) {
                        DockingPanel p = ((IconButton) c).getDockingPanel();
                        if (dock.equals(p)) {
                            eastButtonBar.remove(c);
                            removedEast = true;
                            break;
                        }
                    }
                }
                if (!removedEast) {
                    throw new IllegalStateException("DockingPanel \""
                            + dock.getTitle() + "\" was not previously "
                            + "iconfied to the EAST");
                }
                break;
            case SOUTH:
                boolean removedSouth = false;
                for (Component c : southButtonBar.getComponents()) {
                    if (c instanceof IconButton) {
                        DockingPanel p = ((IconButton) c).getDockingPanel();
                        if (dock.equals(p)) {
                            southButtonBar.remove(c);
                            removedSouth = true;
                            break;
                        }
                    }
                }
                if (!removedSouth) {
                    throw new IllegalStateException("DockingPanel \""
                            + dock.getTitle() + "\" was not previously "
                            + "iconfied to the SOUTH");
                }
                break;
            case WEST:
                boolean removedWest = false;
                for (Component c : westButtonBar.getComponents()) {
                    if (c instanceof IconButton) {
                        DockingPanel p = ((IconButton) c).getDockingPanel();
                        if (dock.equals(p)) {
                            westButtonBar.remove(c);
                            removedWest = true;
                            break;
                        }
                    }
                }
                if (!removedWest) {
                    throw new IllegalStateException("DockingPanel \""
                            + dock.getTitle() + "\" was not previously "
                            + "iconfied to the WEST");
                }
                break;
        }
    }

    private void addAnalytic(DockingPanel dock) {
        if (analyticTabs == null) {
            analyticTabs = new javax.swing.JTabbedPane();
            analyticTabs.setName("analyticTabs");
            analyticTabs.setMinimumSize(new Dimension(100, 100));
            analyticTabs.setBorder(BORDER);
        }

        analyticTabs.add(dock);
        configureTabComponent(analyticTabs, dock);
        getDockingPane().add(analyticTabs, "analytics");
        getDockingPane().getMultiSplitLayout().displayNode("analytics", true);
    }

    private void removeAnalytic(DockingPanel dock) {
        if (!analytics.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Analytics Dock.");
        }
        analytics.remove(dock);
        if (analyticTabs != null) {
            analyticTabs.remove(dock);
            analyticTabs.updateUI();
        }
        if (analyticTabs.getTabCount() == 0) {
            getDockingPane().remove(analyticTabs);
            getDockingPane().getMultiSplitLayout().displayNode("analytics", false);
            analyticTabs.setVisible(false);
            analyticTabs = null;
        }
    }

    private void addBrowser(DockingPanel dock) {
        if (browserTabs == null) {
            browserTabs = new javax.swing.JTabbedPane();
            browserTabs.setName("browserTabs");
            browserTabs.setMinimumSize(new Dimension(150, 150));
            browserTabs.setBorder(BORDER);
        }

        browserTabs.add(dock);
        configureTabComponent(browserTabs, dock);
        browserTabs.setVisible(true);
        getDockingPane().add(browserTabs, "browsers");
        getDockingPane().getMultiSplitLayout().displayNode("browsers", true);
    }

    private void removeBrowser(DockingPanel dock) {
        if (!browsers.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Browsers Dock.");
        }
        browsers.remove(dock);
        if (browserTabs != null) {
            browserTabs.remove(dock);
            browserTabs.updateUI();
        }
        if (browserTabs.getTabCount() == 0) {
            getDockingPane().remove(browserTabs);
            getDockingPane().getMultiSplitLayout().displayNode("browsers", false);
            browserTabs.setVisible(false);
            browserTabs = null;
        }
    }

    private void addEditor(DockingPanel dock) {
        if (editorTabs == null) {
            editorTabs = new javax.swing.JTabbedPane();
            editorTabs.setBorder(BORDER);
        }

        editorTabs.add(dock);
        configureTabComponent(editorTabs, dock);
        editorTabs.setVisible(true);
        getDockingPane().add(editorTabs, "editors");
    }

    private void removeEditor(DockingPanel dock) {
        if (!editors.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Editors Dock.");
        }
        editors.remove(dock);
        if (editorTabs != null) {
            editorTabs.remove(dock);
            editorTabs.updateUI();
        }
        if (editorTabs.getTabCount() == 0) {
            getDockingPane().remove(editorTabs);
            editorTabs.setVisible(false);
            editorTabs = null;
        }
    }

    private void addStat(DockingPanel dock) {
        if (statTabs == null) {
            statTabs = new javax.swing.JTabbedPane();
            statTabs.setName("statTabs");
            statTabs.setMinimumSize(new Dimension(150, 150));
            statTabs.setBorder(BORDER);
        }

        statTabs.add(dock);
        configureTabComponent(statTabs, dock);
        statTabs.setVisible(true);
        getDockingPane().add(statTabs, "stats");
        getDockingPane().getMultiSplitLayout().displayNode("stats", true);
    }

    private void removeStat(DockingPanel dock) {
        if (!stats.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Statistics Dock.");
        }
        stats.remove(dock);
        if (statTabs != null) {
            statTabs.remove(dock);
            statTabs.updateUI();
        }
        if (statTabs.getTabCount() == 0) {
            getDockingPane().remove(statTabs);
            getDockingPane().getMultiSplitLayout().displayNode("stats", false);
            statTabs.setVisible(false);
            statTabs = null;
        }
    }

    private void configureTabComponent(javax.swing.JTabbedPane pane, DockingPanel dock) {
        int idx = pane.indexOfComponent(dock);
        if (idx > -1) {
            pane.setTabComponentAt(idx, new TabComponent(pane, this, dock));
        }
    }

    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();

        menuBar.setName("menuBar"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap
                = org.jdesktop.application.Application.getInstance()
                        .getContext().getResourceMap(DockingView.class);
        jMenu1.setText(resourceMap.getString("jMenu1.text")); // NOI18N
        jMenu1.setName("jMenu1"); // NOI18N

        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText(resourceMap.getString("jMenuItem4.text")); // NOI18N
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jSeparator1.setName("jSeparator1"); // NOI18N
        jMenu1.add(jSeparator1);

        jMenuItem5.setText(resourceMap.getString("jMenuItem5.text")); // NOI18N
        jMenuItem5.setName("jMenuItem5"); // NOI18N
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        menuBar.add(jMenu1);

        setMenuBar(menuBar);
        component = new JPanel(new BorderLayout());
        component.setName("component");

        dockingPane = new MultiSplitPane();

        eastButtonBar = new JPanel(new GridLayout(7, 1));
        eastButtonBar.setName("eastButtonBar");

        southButtonBar = new JPanel(new GridLayout(1, 10));
        southButtonBar.setName("southButtonBar");

        westButtonBar = new JPanel(new GridLayout(7, 1));
        westButtonBar.setName("westButtonBar");

        layout = dockingPane.getMultiSplitLayout();
        layout.layoutByWeight(dockingPane);
        modelRoot = null;
        XMLDecoder in = null;
        try {
            in = new XMLDecoder(
                    getContext()
                            .getLocalStorage()
                            .openInputFile(
                                    StorageLocations.CONFIG_DIR,
                                    sessionFileName
                            )
            );

            modelRoot = (MultiSplitLayout.Node) in.readObject();
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to restore the docking session "
                    + "state from \"{0}{1}{2}\"", ex, configPath,
                    File.separator, sessionFileName);
        } finally {
            if (in != null) {
                in.close();
            }
        }

        if (modelRoot == null) {
            modelRoot = layoutDefinition == null
                    ? MultiSplitLayout.parseModel(layoutDefinition)
                    : MultiSplitLayout.parseModel(defaultLayoutDefinition);
        } else {
            layout.setFloatingDividers(false);
            dockingPane.setPreferredSize(modelRoot.getBounds().getSize());
        }

        layout.setModel(modelRoot);
        layout.setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        layout.addPropertyChangeListener(this);
        
        if (layout.getNodeForName("editors") != null) {
            layout.getNodeForName("editors").setWeight(1.0d);
            layout.getNodeForName("editors").getParent().setWeight(1.0d);
        }

        component.add(eastButtonBar, BorderLayout.EAST);
        component.add(southButtonBar, BorderLayout.SOUTH);
        component.add(westButtonBar, BorderLayout.WEST);
        component.add(dockingPane, BorderLayout.CENTER);

        setComponent(component);
    }

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
        addDock(new BrowserPanel(getApplication()));
    }

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {
        addDock(new AnalyticPanel(getApplication()));
    }

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {
        addDock(new StatsPanel(getApplication()));
    }

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {
        addDock(new EditorPanel(getApplication()));
    }

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {
        getApplication().exit(SysExits.EX_OK);
    }

    private static final String defaultLayoutDefinition
            = "(ROW weight=0.0 (LEAF name=browsers weight=0.0) "
            + "(COLUMN weight=1.0"
            + "(ROW weight=1.0 (LEAF name=editors weight=1.0) "
            + "(LEAF name=stats weight=0.0))"
            + "weight=0.0 (LEAF name=analytics weight=0.0)))";
    private static final String sessionFileName = "docking-session.xml";
    private static final java.awt.Color BORDER_COLOR
            = ColorUtils.isDarkColor(UIManager.getColor("Button.background"))
            ? Color.darkGray
            : Color.lightGray;
    private static final javax.swing.border.Border BORDER
            = javax.swing.BorderFactory.createLineBorder(BORDER_COLOR, 1, true);

    private static final float QUARTER = 0.25f;
    private static final float HALF = 2 * QUARTER;
    private static final float THREE_QUARTERS = 3 * QUARTER;

    private final String configPath;

    private final Logger logger;
    private final ActionMap actionMap;
    private static ExternalFrame extFrame;

    private final String layoutDefinition;
    private final int initBrowserWidth;
    private final int initAnalyticHeight;

    private MultiSplitPane dockingPane;
    private MultiSplitLayout layout;
    private MultiSplitLayout.Node modelRoot;

    private JPanel component;   // The component that gets added to the View.
    private JPanel eastButtonBar;
    private JPanel southButtonBar;
    private JPanel westButtonBar;

    private final List<DockingPanel> analytics;
    private final List<DockingPanel> browsers;
    private final List<DockingPanel> editors;
    private final List<DockingPanel> stats;

    private javax.swing.JTabbedPane analyticTabs;
    private javax.swing.JTabbedPane browserTabs;
    private javax.swing.JTabbedPane editorTabs;
    private javax.swing.JTabbedPane statTabs;

    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuBar menuBar;

}
