/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   IconButton.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 6, 2023
 *  Modified   :   Jan 6, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 6, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import org.jdesktop.docking.DockingPanel;
import org.jdesktop.docking.DockingView.IconDirection;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class IconButton extends javax.swing.JButton {

    private static final long serialVersionUID = 4016717679522238515L;
    
    private final DockingPanel dock;
    private final IconDirection direction;
    
    public IconButton (DockingPanel dock, IconDirection direction) {
        this.dock = dock;
        this.direction = direction;
    }
    
    public DockingPanel getDockingPanel() {
        return dock;
    }
    
    public IconDirection getIconDirection() {
        return direction;
    }

}
