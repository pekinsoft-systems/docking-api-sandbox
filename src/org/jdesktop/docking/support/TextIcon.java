/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   TextIcon.java
 *  Author     :   Rob Camick
 *  Created    :   Apr 6, 2009
 *  Modified   :   Jan 4, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 6, 2009   Rob Camick           Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;

/**
 * The `TextIcon` will paint a {@link java.lang.String} of text as an {@link 
 * javax.swing.Icon Icon}. The `Icon` can be used by any Swing component that
 * supports icons.
 * 
 * `TextIcon` supports two different layout styles:
 * 
 * - `HORIZONTAL` &mdash; does normal rendering of the text by using the
 * {@link java.awt.Graphics#drawString(java.lang.String, int, int) 
 * `Graphics.drawString(...)} method
 * - `VERTICAL` &mdash; each character is displayed on a separate line
 * 
 * `TextIcon` was designed to be rendered on a specific {@link 
 * javax.swing.JComponent JComponent} as it requires {@link java.awt.FontMetrics
 * FontMetrics} information in order to calculate its size and to do the 
 * rendering. Therefore, it should only be added to the component for which it
 * was created.
 * 
 * By default the text will be rendered using the {@link java.awt.Font Font} and
 * foreground color of its associated component. HOwever, this class does allow
 * you to override these properties. Also, starting in JDK6, the desktop 
 * rendering hints will be used to render the text. For versions not supporting
 * the rendering hints, antialiasing will be turned on.
 * 
 * This class was originally written by Rob Camick for a blog he wrote at
 * <a href="https://tips4java.wordpress.com/2009/04/02/text-icon/">Java Tips
 * Weblog</a>. The code was freely offered up for anyone to use, with no 
 * licensing information included. Our thanks go to Rob for his awesome work, as
 * it will be very useful to us in our docking library endeavor. Any alterations
 * to the original code within this class will be noted by inline comments, 
 * marked with the name of the developer that made the change, as well as the
 * date the change was made. This same information (in brief) will be included
 * in the changelog at the top of this class.
 *
 * @author Rob Camick
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt; &mdash; Modified.
 * 
 * @version 1.0
 * @since 1.0
 */
public class TextIcon implements Icon, PropertyChangeListener {
    
    public enum Layout { HORIZONTAL, VERTICAL }
    
    /**
     * Constructs a new `TextIcon` instance using the default horizontal layout.
     * The resulting icon will look no different than entering text onto the
     * {@link javax.swing.JComponent JComponent}.
     * 
     * This is a convenience constructor for not needing to add the
     * `Layout.HORIZONTAL` layout enumeration constant, since this is the most
     * common way of creating a `TextIcon`.
     * 
     * @param component the `JComponent` on which the icon should be placed
     * @param text  the text that will make up the icon
     */
    public TextIcon (JComponent component, String text) {
        this(component, text, Layout.HORIZONTAL);
    }
    
    /**
     * Constructs a new `TextIcon` instance as defined by all provided properties.
     * 
     * @param component the `JComponent` on which the icon should be placed
     * @param text the text that will make up the icon
     * @param layout the layout direction of the text &mdash; ***must be one of
     * the `Layout` enumeration constants***: `HORIZONTAL` or `VERTICAL`
     */
    public TextIcon (JComponent component, String text, Layout layout) {
        this.component = component;
        this.layout = layout;
        setText(text);
        
        component.addPropertyChangeListener("font", this);
    }
    
    /**
     * Retrieves the `Layout` being used.
     * 
     * @return the current `Layout`
     */
    public Layout getLayout() {
        return layout;
    }
    
    /**
     * Retrieves the text of the icon.
     * 
     * @return the icon's text value
     */
    public String getText() {
        return text;
    }
    
    /**
     * Sets the text to be rendered on the icon.
     * 
     * @param text the icon's new text
     */
    public final void setText(String text) {
        this.text = text;
    }
    
    /**
     * Retrieves the {@link java.awt.Font Font} currently being used to render
     * the text in the {@link javax.swing.Icon Icon}.
     * 
     * @return the current `Font`
     */
    public Font getFont() {
        return font;
    }
    
    /**
     * Sets the {@link java.awt.Font Font} to be used for rendering the text in
     * the {@link javax.swing.Icon Icon}.
     * 
     * @param font the new `Font` to be used
     */
    public void setFont(Font font) {
        this.font = font;
        
        calculateIconDimensions();
    }
    
    /**
     * Retrieves the {@link java.awt.Color Color} currently being used as the
     * foreground color in which the text is being rendered.
     * 
     * @return the current foreground `Color`
     */
    public Color getForeground() {
        if (foreground == null) {
            return component.getForeground();
        }
        
        return foreground;
    }
    
    /**
     * Sets the new foreground {@link java.awt.Color Color} to be used for 
     * rendering the text.
     * 
     * @param foreground the new foreground `Color`
     */
    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }
    
    /**
     * Retrieves the current padding used when rendering the text.
     * 
     * @return the current padding (in pixels)
     */
    public int getPadding() {
        return padding;
    }
    
    /**
     * Sets the new padding to use when rendering the icon. Specify the value in
     * pixels.
     * 
     * @param padding the new padding (in pixels)
     */
    public void setPadding(int padding) {
        this.padding = padding;
        
        calculateIconDimensions();
    }

    /**
     * Paint the text of this icon at the specified location.
     * 
     * @param c the component to which the icon is added
     * @param g the graphics context
     * @param x the X coordinate of the icon's top-left corner
     * @param y the Y coordinate of the icon's top-left corner
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g.create();
        
        // The "desktophints" is supported since JDK6
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Map map = (Map) toolkit.getDesktopProperty("awt.font.desktophints");
        
        if (map != null) {  // We are on at least JDK6.
            g2.addRenderingHints(map);
        } else {
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, 
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        
        g2.setFont(getFont());
        g2.setColor(getForeground());
        FontMetrics fm = g2.getFontMetrics();
        
        if (layout == Layout.HORIZONTAL) {
            g2.translate(x, y + fm.getAscent());
            g2.drawString(text, padding, 0);
        } else if (layout == Layout.VERTICAL) {
            int offsetY = fm.getAscent() - fm.getDescent() + padding;
            int incrementY = fm.getHeight() - fm.getDescent();
            
            for (int i = 0; i < strings.length; i++) {
                int offsetX = Math.round((getIconWidth() - stringWidths[i]) / 2.0f);
                g2.drawString(strings[i], x + offsetX, y + offsetY);
                offsetY += incrementY;
            }
        }
        
        g2.dispose();
    }

    @Override
    public int getIconWidth() {
        return iconWidth;
    }

    @Override
    public int getIconHeight() {
        return iconHeight;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // Handle font change when using the default font.
        if (font == null) {
            calculateIconDimensions();
        }
    }
    
    private void calculateIconDimensions() {
        Font font = getFont();
        FontMetrics fm = component.getFontMetrics(font);
        
        if (layout == Layout.HORIZONTAL) {
            iconWidth = fm.stringWidth(text) + (padding * 2);
            iconHeight = fm.getHeight();
        } else if (layout == Layout.VERTICAL) {
            int maxWidth = 0;
            strings = new String[text.length()];
            stringWidths = new int[text.length()];
            
            // Find the widest character in the text string.
            for (int i = 0; i < text.length(); i++) {
                strings[i] = text.substring(i, i + 1);
                stringWidths[i] = fm.stringWidth(strings[i]);
                maxWidth = Math.max(maxWidth, stringWidths[i]);
            }
            
            // Add a minimum of 2 extra pixels, plus the leading value on each
            //+ side of the character.
            iconWidth = maxWidth + ((fm.getLeading() + 2) * 2);
            
            // Decrease the normal gap between lines of text by taking into
            //+ account the descent.
            iconHeight = (fm.getHeight()) - fm.getDescent() * text.length();
            iconHeight += padding * 2;
        }
        
        component.revalidate();
    }
    
    private JComponent component;
    private Layout layout;
    private String text;
    private Font font;
    private Color foreground;
    private int padding;
    
    // Used for the implementation of the Icon interface:
    private int iconWidth;
    private int iconHeight;
    
    // Used for Layout.VERTICAL to save reparsing the text each time the icon is
    //+ repainted.
    private String[] strings;
    private int[] stringWidths;

}
