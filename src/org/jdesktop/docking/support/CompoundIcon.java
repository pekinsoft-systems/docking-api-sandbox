/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   CompoundIcon.java
 *  Author     :   Rob Camick
 *  Created    :   Mar 29, 2009
 *  Modified   :   Jan 04, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2009  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

/**
 * The `CompoundIcon` will paint two or more {@link javax.swing.Icon Icons} as a
 * single `Icon`. The `Icon`s are painted in the order in which they are added.
 * 
 * The `Icon`s are laid out on the specified axis:
 * 
 * - X-Axis (horizontally)
 * - Y-Axis (vertically)
 * - Z-Axis (stacked)
 * 
 * This class was originally written by Rob Camick for a blog he wrote at
 * <a href="https://tips4java.wordpress.com/2009/04/02/text-icon/">Java Tips
 * Weblog</a>. The code was freely offered up for anyone to use, with no 
 * licensing information included. Our thanks go to Rob for his awesome work, as
 * it will be very useful to us in our docking library endeavor. Any alterations
 * to the original code within this class will be noted by inline comments, 
 * marked with the name of the developer that made the change, as well as the
 * date the change was made. This same information (in brief) will be included
 * in the changelog at the top of this class.
 *
 * @author Rob Camick
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt; &mdash; Modified.
 * 
 * @version 1.0
 * @since 1.0
 */
public class CompoundIcon implements Icon {
    
    public enum Axis { X_AXIS, Y_AXIS, Z_AXIS }
    
    public final static float TOP = 0.0f;
    public final static float LEFT = 0.0f;
    public final static float CENTER = 0.5f;
    public final static float BOTTOM = 1.0f;
    public final static float RIGHT = 1.0f;
    
    /**
     * Constructs a `CompoundIcon` instance on the default `Axis.X_AXIS` with the
     * default gap of zero (0) and center alignment.
     * 
     * @param icons the {@link javax.swing.Icon Icons} to be painted as part of
     * this `CompoundIcon`
     */
    public CompoundIcon (Icon... icons) {
        this(Axis.X_AXIS, icons);
    }
    
    /**
     * Constructs a `CompoundIcon` instance on the specified `Axis`, with the
     * default gap of zero (0) and center alignment.
     * 
     * @param axis the `Axis` on which to paint the {@link javax.swing.Icon Icons}
     * @param icons the `Icon`s to be painted as part of this `CompoundIcon`
     */
    public CompoundIcon(Axis axis, Icon... icons) {
        this(axis, 0, icons);
    }
    
    /**
     * Constructs a `CompoundIcon` instance on the specified `Axis`, with the
     * specified gap and center alignment.
     * 
     * @param axis the `Axis` on which to paint the {@link javax.swing.Icon Icons}
     * @param gap the gap between the `Icon`s
     * @param icons the `Icon`s to be painted as part of this `CompoundIcon`
     */
    public CompoundIcon(Axis axis, int gap, Icon... icons) {
        this(axis, gap, CENTER, CENTER, icons);
    }
    
    /**
     * Constructs a fully specified `CompoundIcon` instance.
     * 
     * @param axis the `Axis` on which to paint the {@link javax.swing.Icon Icons}
     * @param gap the gap between the `Icon`s
     * @param alignmentX the X (horizontal) alignment of the `Icon`s &mdash;
     * common values are `LEFT`, `CENTER`, and `RIGHT`, but can be any value
     * between 0.0 and 1.0
     * @param alignmentY the Y (vertical) alignment of the `Icon`s &mdash;
     * common values are `LEFT`, `CENTER`, and `RIGHT`, but can be any value
     * between 0.0 and 1.0
     * @param icons the `Icon`s to be painted as part of this `CompoundIcon`
     */
    public CompoundIcon(Axis axis, int gap, float alignmentX, float alignmentY,
            Icon... icons) {
        this.axis = axis;
        this.gap = gap;
        this.alignmentX = alignmentX;
        this.alignmentY = alignmentY;
        for (int i = 0; i < icons.length; i++) {
            if (icons[i] == null) {
                String message = "Icon (" + i + ") cannot be null";
                throw new IllegalArgumentException(message);
            }
        }
        this.icons = icons;
    }

    /**
     *  Get the Axis along which each icon is painted.
     *
     *  @return the Axis
    */
    public Axis getAxis()
    {
            return axis;
    }

    /**
     *  Get the gap between each icon
     *
     *  @return the gap in pixels
     */
    public int getGap()
    {
            return gap;
    }

    /**
     *  Get the alignment of the icon on the x-axis
     *
     *  @return the alignment
     */
    public float getAlignmentX()
    {
            return alignmentX;
    }

    /**
     *  Get the alignment of the icon on the y-axis
     *
     *  @return the alignment
     */
    public float getAlignmentY()
    {
            return alignmentY;
    }

    /**
     *  Get the number of Icons contained in this CompoundIcon.
     *
     *  @return the total number of Icons
     */
    public int getIconCount()
    {
            return icons.length;
    }

    /**
     *  Get the Icon at the specified index.
     *
     *  @param index  the index of the Icon to be returned
     *  @return  the Icon at the specifed index
     *  @exception IndexOutOfBoundsException  if the index is out of range
     */
    public Icon getIcon(int index)
    {
            return icons[ index ];
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (null == axis) {    // Must be Z_AXIS.
            int width = getIconWidth();
            int height = getIconHeight();
            
            for (Icon icon : icons) {
                int iconX = getOffset(width, icon.getIconWidth(), alignmentX);
                int iconY = getOffset(height, icon.getIconHeight(), alignmentY);
                icon.paintIcon(c, g, x + iconX, y + iconY);
            }
        } else switch (axis) {
            case X_AXIS:
                {
                    int height = getIconHeight();
                    for (Icon icon : icons) {
                        int iconY = getOffset(height, icon.getIconHeight(), alignmentY);
                        icon.paintIcon(c, g, x, y + iconY);
                        x += icon.getIconWidth() + gap;
                    }       break;
                }
            case Y_AXIS:
                {
                    int width = getIconWidth();
                    for (Icon icon : icons) {
                        int iconX = getOffset(width, icon.getIconWidth(), alignmentX);
                        icon.paintIcon(c, g, x + iconX, y);
                        y += icon.getIconHeight() + gap;
                    }       break;
                }
            default:
                {
                        // Must be Z_AXIS.
                        int width = getIconWidth();
                        int height = getIconHeight();
                        for (Icon icon : icons) {
                                int iconX = getOffset(width, icon.getIconWidth(), alignmentX);
                                int iconY = getOffset(height, icon.getIconHeight(), alignmentY);
                                icon.paintIcon(c, g, x + iconX, y + iconY);
                                }       break;
                }
        }
    }

    @Override
    public int getIconWidth() {
        int width = 0;
        
        if (axis == Axis.X_AXIS) {
            // Add the width of all Icons while also including the gap.
            width += (icons.length - 1) * gap;
            for (Icon icon : icons) {
                width += icon.getIconWidth();
            }
        } else {
            // Just find the maximum width.
            for (Icon icon : icons) {
                width = Math.max(width, icon.getIconWidth());
            }
        }
        
        return width;
    }

    @Override
    public int getIconHeight() {
        int height = 0;
        
        if (axis == Axis.Y_AXIS) {
            // Add the height of all Icons while also including the gap.
            height += (icons.length - 1) * gap;
            
            for (Icon icon : icons) {
                height += icon.getIconHeight();
            }
        } else {
            // Just find the maximum height.
            for (Icon icon : icons) {
                height = Math.max(height, icon.getIconHeight());
            }
        }
        
        return height;
    }
    
    /* When the icon value is smaller than the maximum value of all icons, the
     * icon needs to be aligned appropriately. Calculate the offset to be used
     * when painting the icon to achieve proper alignment.
     */
    private int getOffset(int maxValue, int iconValue, float alignment) {
        float offset = (maxValue - iconValue) * alignment;
        return Math.round(offset);
    }
    
    private Icon[] icons;
    private Axis axis;
    private int gap;
    private float alignmentX = CENTER;
    private float alignmentY = CENTER;
    
}
