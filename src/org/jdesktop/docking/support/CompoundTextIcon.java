/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   CompoundTextIcon.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 6, 2023
 *  Modified   :   Jan 6, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 6, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * `CompoundTextIcon` is a subclass of {@link org.jdesktop.docking.support.CompoundIcon
 * CompoundIcon} that is specifically used to create a horizontal icon that
 * mimics the appearance of the icon and text on a standard {@link 
 * javax.swing.JButton JButton} for rotation purposes.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class CompoundTextIcon extends CompoundIcon {
    
    private final int defaultGap = 4;
    
    private Icon icon;
    private Icon textIcon;
    private int gap;
    private float alignmentY;
    
    /**
     * Convenience constructor for times when the {@link javax.swing.JButton
     * JButton} already exists, but needs to be rotated, so a `CompoundTextIcon`
     * is needed.
     * 
     * @param button the button that will eventually be rotated.
     */
    public CompoundTextIcon (JButton  button) {
        this(button.getIcon(), button.getText(), button.getIconTextGap());
    }
    
    /**
     * Convenience constructor for times when the {@link javax.swing.Icon Icon}
     * is on its own, and the {@link org.jdesktop.docking.support.TextIcon
     * TextIcon} has already been created.
     * 
     * @param icon the icon image for the button
     * @param textIcon the text for the button as a `TextIcon`
     */
    public CompoundTextIcon(Icon icon, Icon textIcon) {
        this.icon = icon;
        this.textIcon = textIcon;
        this.gap = defaultGap;
    }
    
    /**
     * Convenience constructor for times when the {@link javax.swing.Icon Icon}
     * and text is all that is available.
     * 
     * @param icon the icon image for the button
     * @param text the text for the button
     */
    public CompoundTextIcon(Icon icon, String text) {
        this(icon, new TextIcon(new JButton(text, icon), text));
    }
    
    /**
     * Standard constructor for when all that is available is the icon image and
     * the text.
     * 
     * @param icon the icon image for the button
     * @param text the text displayed on the button
     * @param gap the gap that should be placed between the icon and text
     */
    public CompoundTextIcon(Icon icon, String text, int gap) {
        this(icon, text);
        this.gap = gap;
    }
    
    public Icon getIcon() {
        return icon;
    }
    
    public Icon getTextIcon() {
        return textIcon;
    }
    
    @Override
    public int getIconHeight() {
        return Math.max(icon.getIconHeight(), textIcon.getIconHeight());
    }
    
    @Override
    public int getIconWidth() {
        return icon.getIconWidth() + gap + textIcon.getIconWidth();
    }
    
    @Override
    public int getGap() {
        return gap;
    }
    
    @Override
    public float getAlignmentY() {
        return alignmentY;
    }
    
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int maxHeight = Math.max(icon.getIconHeight(), textIcon.getIconHeight());
        int totalWidth = icon.getIconWidth() + gap + textIcon.getIconWidth();
        int offsetY = (((int)(y / 2.5)) - (maxHeight - textIcon.getIconHeight()) / 2) + 4;
        
        icon.paintIcon(c, g, x, y);
        x += icon.getIconWidth() + gap;
        textIcon.paintIcon(c, g, x, offsetY);
    }

}
