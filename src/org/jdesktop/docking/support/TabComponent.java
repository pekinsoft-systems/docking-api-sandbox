/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   TabComponent.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 4, 2023
 *  Modified   :   Jan 4, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 4, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.jdesktop.docking.DockingPanel;
import org.jdesktop.docking.DockingView;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class TabComponent extends JPanel {
    
    public TabComponent (JTabbedPane pane, DockingView view, DockingPanel dock) {
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) {
            throw new IllegalArgumentException("null pane");
        }
        if (view == null) {
            throw new IllegalArgumentException("null view");
        }
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        
        this.pane = pane;
        this.view = view;
        this.dock = dock;
        
        initComponents();
    }
    
    public DockingView getView() {
        return view;
    }
    
    public JTabbedPane getTabbedPane() {
        return pane;
    }
    
    public DockingPanel getDock() {
        return dock;
    }
    
    private void initComponents() {
        setOpaque(false);
        JLabel label = new JLabel();
        label.setText(dock.getTitle());
        label.setIcon(dock.getIcon());
        add(label);
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        
        if (dock.isExternalizable()) {
            add(new ExternalizeTabButton(view, dock));
        }
        if (dock.isIconifiable()) {
            add(new IconifyTabButton(view, dock));
        }
        if (dock.isMaximizable()) {
            add(new MaximizeTabButton(view, dock));
        }
        if (dock.isClosable()) {
            add(new CloseTabButton(view, dock));
        }
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    }
    
    private final JTabbedPane pane;
    private final DockingView view;
    private final DockingPanel dock;

}
