/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   RotatedIcon.java
 *  Author     :   Rob Camick
 *  Created    :   Apr 06, 2009
 *  Modified   :   Jan 4, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 06, 2009  Rob Camick           Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;

/**
 * 
 * 
 * This class was originally written by Rob Camick for a blog he wrote at
 * <a href="https://tips4java.wordpress.com/2009/04/02/text-icon/">Java Tips
 * Weblog</a>. The code was freely offered up for anyone to use, with no 
 * licensing information included. Our thanks go to Rob for his awesome work, as
 * it will be very useful to us in our docking library endeavor. Any alterations
 * to the original code within this class will be noted by inline comments, 
 * marked with the name of the developer that made the change, as well as the
 * date the change was made. This same information (in brief) will be included
 * in the changelog at the top of this class.
 *
 * @author Rob Camick
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt; &mdash; Modified.
 * 
 * @version 1.0
 * @since 1.0
 */
public class RotatedIcon implements Icon {
    
    public enum Rotate { DOWN, UP, UPSIDE_DOWN, ABOUT_CENTER }
    
    /**
     * Constructs a new `RotatedIcon` instance for the given {@link 
     * javax.swing.Icon Icon} with the default rotation of `Rotate.UP`.
     * 
     * @param icon the `Icon` to rotate
     */
    public RotatedIcon (Icon icon) {
        this(icon, Rotate.UP);
    }
    
    /**
     * Constructs a new `RotatedIcon` instance for the given {@link 
     * javax.swing.Icon Icon} and the specified rotation.
     * 
     * @param icon the `Icon` to rotate
     * @param rotate the rotation to apply &mdash; ***must be one of** the 
     * `Rotate` enumeration constants: `DOWN`, `UP`, `UPSIDE_DOWN`, or
     * `ABOUT_CENTER`
     */
    public RotatedIcon (Icon icon, Rotate rotate) {
        this.icon = icon;
        this.rotate = rotate;
    }
    
    /**
     * Constructs a new `RotatedIcon` instance for the given {@link 
     * javax.swing.Icon Icon} to be rotated the specified number of degrees. This
     * constructor automatically sets the `Icon` to be rotated about its center.
     * 
     * @param icon the `Icon` to rotate
     * @param degrees the degrees of rotation
     */
    public RotatedIcon (Icon icon, double degrees) {
        this(icon, degrees, false);
    }
    
    /**
     * Constructs a new `RotatedIcon` instance for the given {@link 
     * javax.swing.Icon Icon} to be rotated the specified number of degrees and
     * whether or not to rotate it about its center.
     * 
     * @param icon the `Icon` to rotate
     * @param degrees the degrees of rotation
     * @param circularIcon whether or not to rotate around the center of the 
     * `Icon` (prevents its size from changing)
     */
    public RotatedIcon (Icon icon, double degrees, boolean circularIcon) {
        this(icon, Rotate.ABOUT_CENTER);
        setDegrees(degrees);
        setCircularIcon(circularIcon);
    }

    /**
     *  Gets the Icon to be rotated
     *
     *  @return the Icon to be rotated
     */
    public Icon getIcon()
    {
            return icon;
    }

    /**
     *  Gets the Rotate enum which indicates the direction of rotation
     *
     *  @return the Rotate enum
     */
    public Rotate getRotate()
    {
            return rotate;
    }

    /**
     *  Gets the degrees of rotation. Only used for Rotate.ABOUT_CENTER.
     *
     *  @return the degrees of rotation
     */
    public double getDegrees()
    {
            return degrees;
    }

    /**
     *  Set the degrees of rotation. Only used for Rotate.ABOUT_CENTER.
     *  This method only sets the degress of rotation, it will not cause
     *  the Icon to be repainted. You must invoke repaint() on any
     *  component using this icon for it to be repainted.
     *
     *  @param degrees the degrees of rotation
     */
    public void setDegrees(double degrees)
    {
            this.degrees = degrees;
    }

    /**
     *  Is the image circular or rectangular? Only used for Rotate.ABOUT_CENTER.
     *  When true, the icon width/height will not change as the Icon is rotated.
     *
     *  @return true for a circular Icon, false otherwise
     */
    public boolean isCircularIcon()
    {
            return circularIcon;
    }

    /**
     *  Set the Icon as circular or rectangular. Only used for Rotate.ABOUT_CENTER.
     *  When true, the icon width/height will not change as the Icon is rotated.
     *
     *  @param true for a circular Icon, false otherwise
     */
    public void setCircularIcon(boolean circularIcon)
    {
            this.circularIcon = circularIcon;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        
        int cWidth = icon.getIconWidth() / 2;
        int cHeight = icon.getIconHeight() / 2;
        int xAdjustment = (icon.getIconWidth() % 2) == 0 ? 0 : -1;
        int yAdjustment = (icon.getIconHeight() % 2) == 0 ? 0 : -1;
        
        if (rotate == Rotate.DOWN) {
            g2.translate(x + cHeight, y + cWidth);
            g2.rotate(Math.toRadians(90));
            icon.paintIcon(c, g2, -cWidth, yAdjustment - cHeight);
        } else if (rotate == Rotate.UP) {
            g2.translate(x + cHeight, y + cWidth);
            g2.rotate(Math.toRadians(-90));
            icon.paintIcon(c, g2, xAdjustment - cWidth, -cHeight);
        } else if (rotate == Rotate.UPSIDE_DOWN) {
            g2.translate(x + cHeight, y + cWidth);
            g2.rotate(Math.toRadians(180));
            icon.paintIcon(c, g2, xAdjustment - cWidth, yAdjustment - cHeight);
        } else if (rotate == Rotate.ABOUT_CENTER) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                    RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setClip(x, y, getIconWidth(), getIconHeight());
            g2.translate((getIconWidth() - icon.getIconWidth()) / 2, 
                    (getIconHeight() - icon.getIconHeight()) / 2);
            g2.rotate(Math.toRadians(degrees), x + cWidth, y + cHeight);
            icon.paintIcon(c, g2, x, y);
        }
        
        g2.dispose();
    }

    @Override
    public int getIconWidth() {
        if (rotate == Rotate.ABOUT_CENTER) {
            if (circularIcon) {
                return icon.getIconHeight();
            } else {
                double radians = Math.toRadians(degrees);
                double sin = Math.abs(Math.sin(radians));
                double cos = Math.abs(Math.cos(radians));
                int width = (int) Math.floor(
                        icon.getIconWidth() * cos 
                                + icon.getIconHeight() * sin);
                return width;
            }
        } else if (rotate == Rotate.UPSIDE_DOWN) {
            return icon.getIconWidth();
        }
        
        return icon.getIconHeight();
    }

    @Override
    public int getIconHeight() {
        if (rotate == Rotate.ABOUT_CENTER) {
            if (circularIcon) {
                return icon.getIconWidth();
            } else {
                double radians = Math.toRadians(degrees);
                double sin = Math.abs(Math.sin(radians));
                double cos = Math.abs(Math.cos(radians));
                int height = (int) Math.floor(
                        icon.getIconHeight() * cos 
                                + icon.getIconWidth() * sin);
                return height;
            }
        } else if (rotate == Rotate.UPSIDE_DOWN) {
            return icon.getIconHeight();
        }
        
        return icon.getIconWidth();
    }
    
    private Icon icon;
    private Rotate rotate;
    private double degrees;
    private boolean circularIcon;

}
