/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   TabButton.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicButtonUI;
import org.jdesktop.application.Action;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
abstract class TabButton extends JButton {

    private static final long serialVersionUID = -1632307047200315072L;
    
    public TabButton () {
        initTabButton();
    }
    
    private void initTabButton() {
        int size = 17;
        setPreferredSize(new Dimension(size, size));
//        setToolTipText((String) action.getValue(Action.SHORT_DESCRIPTION));
        
        // This makes sure that the button looks the same regardless of LaF
        setUI(new BasicButtonUI());
        setContentAreaFilled(false);    // Transparent.
        setFocusable(false);    // Focus not needed.
        setBorder(BorderFactory.createEtchedBorder());
        setBorderPainted(false);
        addMouseListener(new ButtonMouseListener());    // Nice rollover effect.
        setRolloverEnabled(true);
    }

    @Override
    public void updateUI() {
        // We do not want the UI to update.
    }

//    @Override
//    protected abstract void paintComponent(Graphics g);
    
    @Action
    public abstract void doAction(ActionEvent evt);
    
    private final class ButtonMouseListener extends MouseAdapter {

        @Override
        public void mouseExited(MouseEvent e) {
            Component c = e.getComponent();
            if (c instanceof AbstractButton) {
                ((AbstractButton) c).setBorderPainted(false);
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            Component c = e.getComponent();
            if (c instanceof AbstractButton) {
                ((AbstractButton) c).setBorderPainted(true);
            }
        }
        
    }

}
