/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   CloseTabButton.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.docking.DockingPanel;
import org.jdesktop.docking.DockingView;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class IconifyTabButton extends TabButton {

    private static final long serialVersionUID = -7514837962099870921L;
    
    public IconifyTabButton (DockingView view, DockingPanel dock) {
        super();
        this.view = view;
        this.dock = dock;
        icon = resourceMap.getImageIcon("doAction.Action.smallIcon");
        setAction(context.getActionMap(this).get("doAction"));
    }
    
    @Action
    public void doCloseTab(ActionEvent evt) {
        view.removeDock(dock);
    }

    @Action
    @Override
    public void doAction(ActionEvent evt) {
//        Component parent = ((Component) evt.getSource()).getParent();
//        JTabbedPane tabs = null;
//        TabComponent tab = null;
//        
//        while (parent != null && (tabs == null || tab == null)) {
//            if (parent instanceof TabComponent) {
//                tab = (TabComponent) parent;
//            } else if (parent instanceof JTabbedPane) {
//                tabs = (JTabbedPane) parent;
//            }
//            
//            parent = parent.getParent();
//        }
        
        view.iconifyDock(dock);
//        if (tabs != null) {
//            int index = tabs.indexOfTabComponent(tab);
//            tabs.remove(index);
//        }
//        view.removeDock(dock);
    }
    
    private static final ApplicationContext context
            = Application
                    .getInstance()
                    .getContext();
    private static final ResourceMap resourceMap
            = context
                    .getResourceMap(IconifyTabButton.class);
    
    private final DockingView view;
    private final DockingPanel dock;
    private final ImageIcon icon;

}
