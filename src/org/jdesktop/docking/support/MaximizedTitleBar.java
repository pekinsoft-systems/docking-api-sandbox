/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   MaximizedTitleBar.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 5, 2023
 *  Modified   :   Jan 5, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 5, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.support;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.Box.Filler;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.jdesktop.docking.DockingPanel;
import org.jdesktop.docking.DockingView;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class MaximizedTitleBar extends JPanel {

    private static final long serialVersionUID = -4876106621384663582L;
    
    private static final Color BACKGROUND 
            = UIManager.getColor("InternalFrame.activeTitleBackground");
    private static final Color FOREGROUND 
            = UIManager.getColor("InternalFrame.activeTitleForeground");
    
    private final DockingView view;
    private final DockingPanel dock;
    
    private JLabel iconLabel;
    private JLabel titleLabel;
    
    private JButton externalizeButton;
    private JButton restoreButton;
    private JButton closeButton;
    
    public MaximizedTitleBar (DockingView view, DockingPanel dock) {
        super();
        if (view == null) {
            throw new IllegalArgumentException("null view");
        }
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        this.view = view;
        this.dock = dock;
        
        initComponents();
    }
    
    private void initComponents() {
        iconLabel = new JLabel(dock.getIcon());
        iconLabel.setName("iconLabel");
        iconLabel.setSize(dock.getIcon().getIconWidth(), 
                dock.getIcon().getIconHeight());
        titleLabel = new JLabel(dock.getTitle());
        titleLabel.setName("titleLabel");
        titleLabel.setSize(titleLabel.getWidth(), iconLabel.getHeight());
        
        externalizeButton = null;
        closeButton = null;
        if (dock.isExternalizable()) {
            externalizeButton = new ExternalizeTabButton(view, dock);
            externalizeButton.setName("externalizeButton");
            externalizeButton.setSize(new Dimension(
                    externalizeButton.getIcon().getIconWidth(),
                    externalizeButton.getIcon().getIconHeight()));
        }
        if (dock.isClosable()) {
            closeButton = new CloseTabButton(view, dock);
            closeButton.setName("closeButton");
            closeButton.setSize(new Dimension(
                    closeButton.getIcon().getIconWidth(),
                    closeButton.getIcon().getIconHeight()));
        }
        
        restoreButton = new RestoreTabButton(view, dock);
        restoreButton.setName("restoreButton");
        restoreButton.setSize(new Dimension(
                restoreButton.getIcon().getIconWidth(),
                restoreButton.getIcon().getIconHeight()));
        
        Dimension size = new Dimension(getClientWidth(), iconLabel.getHeight());
        setSize(size);
        setOpaque(true);
        setBackground(BACKGROUND);
        setForeground(FOREGROUND);
        setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setLayout(new FlowLayout(FlowLayout.LEADING));
        add(iconLabel);
        add(titleLabel);
        Dimension spacerDim = new Dimension(getGlueWidth(), 5);
        Filler glue = new Filler(spacerDim, spacerDim, spacerDim);
        add(glue);
        if (externalizeButton != null) {
            add(externalizeButton);
        }
        add(restoreButton);
        if (closeButton != null) {
            add(closeButton);
        }
    }
    
    private int getClientWidth() {
        return view.getFrame().getRootPane().getInsets().right
                = view.getFrame().getRootPane().getInsets().left;
    }
    
    private int getGlueWidth() {
        int buttonWidths 
                = (externalizeButton != null ? externalizeButton.getWidth() + 1 : 0)
                + restoreButton.getWidth()
                + (closeButton != null ? closeButton.getWidth() + 1 : 0);
        int labelWidths = iconLabel.getWidth() + 1 + titleLabel.getWidth();
        int otherWidths = getInsets().left + getInsets().right;
        
        int glueWidth = (view.getFrame().getRootPane().getInsets().right
                - view.getFrame().getRootPane().getInsets().left) 
                - (buttonWidths + labelWidths + otherWidths);
        
        return glueWidth;
    }

}
