/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   BrowserDock.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 4, 2023
 *  Modified   :   Jan 4, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 4, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.trials;

/**
 * The `StatsDock` interface is a marker interface to let the {@link 
 * org.jdesktop.docking.DockingView DockingView} know in which location the
 * implementing {@link org.jdesktop.docking.DockingPanel DockingPanel} is to be
 * placed. This marker interface is used in lieu of using the {@link 
 * org.jdesktop.docking.DockingPanel.DockingLocations DockingPanel.DockingLocations}
 * enumeration, which will be going away in a future release.
 * 
 * By having the marker interfaces, the {@link org.jdesktop.docking.DockingView
 * #addDock(org.jdesktop.docking.DockingPanel, org.jdesktop.docking.DockingPanel.DockingLocations) 
 * addDock()} method will not require a second parameter, as the `DockingPanel`
 * will be able to tell the `DockingView` exactly where it wants to be docked
 * simply by implementing the appropriate interface.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public interface StatsDock {

}
