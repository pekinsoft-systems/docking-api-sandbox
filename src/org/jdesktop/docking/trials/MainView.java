/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   MainView.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.docking.trials;


import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.UIManager;
import org.jdesktop.application.Application;
import org.jdesktop.application.SysExits;
import org.jdesktop.docking.DockingPanel;
import org.jdesktop.docking.DockingView;
import org.jdesktop.docking.support.TabComponent;
import org.jdesktop.ui.swingx.MultiSplitLayout;
import org.jdesktop.ui.swingx.MultiSplitLayout.Node;
import org.jdesktop.utils.ColorUtils;


/**
 *
 * @author Sean Carrick
 */
public class MainView extends DockingView {
    
    /**
     * Creates new form MainView
     */
    public MainView(Application application) {
        super(application); //, layoutDefinition
        
        analytics = new ArrayList<>();
        browsers = new ArrayList<>();
        editors = new ArrayList<>();
        stats = new ArrayList<>();
        
        initComponents();
    }

    @Override
    public void addDock(DockingPanel dock) {
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        if (dock instanceof BrowserDock) {
            browsers.add(dock);
            addBrowser(dock);
        } else if (dock instanceof AnalyticDock) {
            analytics.add(dock);
            addAnalytic(dock);
        } else if (dock instanceof EditorDock) {
            editors.add(dock);
            addEditor(dock);
        } else if (dock instanceof StatsDock) {
            stats.add(dock);
            addStat(dock);
        } else {
            throw new IllegalArgumentException("Unknown dock type");
        }
        getDockingPane().updateUI();
    }

    @Override
    public void removeDock(DockingPanel dock) {
        if (dock == null) {
            return;
        }
        if (dock instanceof AnalyticDock) {
            removeAnalytic(dock);
        } else if (dock instanceof BrowserDock) {
            removeBrowser(dock);
        } else if (dock instanceof EditorDock) {
            removeEditor(dock);
        } else if (dock instanceof StatsDock) {
            removeStat(dock);
        } else {
            throw new IllegalArgumentException("Unknown dock type");
        }
        getDockingPane().updateUI();
    }

    @Override
    public void iconifyDock(DockingPanel dock) {
        if (dock == null) {
            throw new IllegalArgumentException("null dock");
        }
        if (dock instanceof BrowserDock) {
            addIconButton(dock, IconDirection.WEST);
            removeDock(dock);
        } else if (dock instanceof StatsDock) {
            addIconButton(dock, IconDirection.EAST);
            removeDock(dock);
        } else {
            addIconButton(dock, IconDirection.SOUTH);
            removeDock(dock);
        }
    }

    @Override
    public void maximizeDock(DockingPanel dock) {
        // TODO: Provide implementation in maximizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void externalizeDock(DockingPanel dock) {
        // TODO: Provide implementation in externalizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void internalizeDock(DockingPanel dock) {
        // TODO: Provide implementation in internalizeDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void restoreDock(DockingPanel dock) {
        // TODO: Provide implementation in restoreDock
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        super.propertyChange(evt);  // Let the View handle what it needs.
        
        MultiSplitLayout layout = getDockingPane().getMultiSplitLayout();
        Node node = null;
        
        if (evt.getPropertyName() != null) switch (evt.getPropertyName()) {
            case "browsersAdded":
                
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();

        menuBar.setName("menuBar"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(MainView.class);
        jMenu1.setText(resourceMap.getString("jMenu1.text")); // NOI18N
        jMenu1.setName("jMenu1"); // NOI18N

        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText(resourceMap.getString("jMenuItem4.text")); // NOI18N
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jSeparator1.setName("jSeparator1"); // NOI18N
        jMenu1.add(jSeparator1);

        jMenuItem5.setText(resourceMap.getString("jMenuItem5.text")); // NOI18N
        jMenuItem5.setName("jMenuItem5"); // NOI18N
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        menuBar.add(jMenu1);

        setMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        addDock(new BrowserPanel(getApplication()));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        addDock(new AnalyticPanel(getApplication()));
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        addDock(new StatsPanel(getApplication()));
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        addDock(new EditorPanel(getApplication()));
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        getApplication().exit(SysExits.EX_OK);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void addAnalytic(DockingPanel dock) {
        if (analyticTabs == null) {
            analyticTabs = new javax.swing.JTabbedPane();
            analyticTabs.setBorder(BORDER);
        }
        
        analyticTabs.add(dock);
        configureTabComponent(analyticTabs, dock);
        getDockingPane().add(analyticTabs, "analytics");
            getDockingPane().getMultiSplitLayout().getNodeForName("analytics")
                    .getParent().setVisible(true);
        getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
    }
    
    private void removeAnalytic(DockingPanel dock) {
        if (!analytics.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Analytics Dock.");
        }
        analytics.remove(dock);
        if (analyticTabs != null) {
            analyticTabs.remove(dock);
            analyticTabs.updateUI();
        }
        if (analyticTabs.getTabCount() == 0) {
//            analyticTabs.setPreferredSize(new Dimension(analyticTabs.getWidth(), 0));
            getDockingPane().remove(analyticTabs);
            analyticTabs.setVisible(false);
            analyticTabs = null;
            getDockingPane().getMultiSplitLayout().getNodeForName("analytics")
                    .getParent().setVisible(false);
            getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
        }
    }
    
    private void addBrowser(DockingPanel dock) {
        if (browserTabs == null) {
            browserTabs = new javax.swing.JTabbedPane();
            browserTabs.setBorder(BORDER);
        }
        
        browserTabs.add(dock);
        configureTabComponent(browserTabs, dock);
        browserTabs.setVisible(true);
        getDockingPane().add(browserTabs, "left");
        getDockingPane().getMultiSplitLayout().getNodeForName("left")
                .getParent().setVisible(true);
    }
    
    private void removeBrowser(DockingPanel dock) {
        if (!browsers.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Browsers Dock.");
        }
        browsers.remove(dock);
        if (browserTabs != null) {
            browserTabs.remove(dock);
            browserTabs.updateUI();
        }
        if (browserTabs.getTabCount() == 0) {
//            browserTabs.setPreferredSize(new Dimension(0, browserTabs.getHeight()));
            getDockingPane().remove(browserTabs);
            browserTabs.setVisible(false);
            browserTabs = null;
            getDockingPane().getMultiSplitLayout().getNodeForName("left")
                    .getParent().setVisible(false);
//            getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
        }
    }
    
    private void addEditor(DockingPanel dock) {
        if (editorTabs == null) {
            editorTabs = new javax.swing.JTabbedPane();
            editorTabs.setBorder(BORDER);
        }
        
        editorTabs.add(dock);
        configureTabComponent(editorTabs, dock);
        editorTabs.setVisible(true);
        getDockingPane().add(editorTabs, "right");
//        getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
    }
    
    private void removeEditor(DockingPanel dock) {
        if (!editors.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Editors Dock.");
        }
        editors.remove(dock);
        if (editorTabs != null) {
            editorTabs.remove(dock);
            editorTabs.updateUI();
        }
        if (editorTabs.getTabCount() == 0) {
            getDockingPane().remove(editorTabs);
            editorTabs.setVisible(false);
            editorTabs = null;
//            getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
        }
    }
    
    private void addStat(DockingPanel dock) {
        if (statTabs == null) {
            statTabs = new javax.swing.JTabbedPane();
            statTabs.setBorder(BORDER);
        }
        
        statTabs.add(dock);
        configureTabComponent(statTabs, dock);
        statTabs.setVisible(true);
        getDockingPane().add(statTabs, "stats");
        getDockingPane().getMultiSplitLayout().displayNode("stats", true);
        getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
    }
    
    private void removeStat(DockingPanel dock) {
        if (!stats.contains(dock)) {
            throw new IllegalStateException("DockingPanel \"" + dock.getTitle()
                    + "\" was never added to the Statistics Dock.");
        }
        stats.remove(dock);
        if (statTabs != null) {
            statTabs.remove(dock);
            statTabs.updateUI();
        }
        if (statTabs.getTabCount() == 0) {
//            statTabs.setPreferredSize(new Dimension(0, statTabs.getHeight()));
            getDockingPane().remove(statTabs);
            statTabs.setVisible(false);
            statTabs = null;
            getDockingPane().getMultiSplitLayout().displayNode("stats", false);
            getDockingPane().getMultiSplitLayout().layoutByWeight(getDockingPane());
        }
    }
    
    private void configureTabComponent(javax.swing.JTabbedPane pane, DockingPanel dock) {
        int idx = pane.indexOfComponent(dock);
        if (idx > -1) {
            pane.setTabComponentAt(idx, new TabComponent(pane, this, dock));
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuBar menuBar;
    // End of variables declaration//GEN-END:variables
    
    private static final String layoutDefinition 
            = "(ROW browsers "
            + "(COLUMN (ROW editors stats) "
            + "analytics))";
    private static final java.awt.Color BORDER_COLOR
            = ColorUtils.isDarkColor(UIManager.getColor("Button.background"))
            ? Color.darkGray
            : Color.lightGray;
    private static final javax.swing.border.Border BORDER
            = javax.swing.BorderFactory.createLineBorder(BORDER_COLOR, 1, true);
    
    private static final float QUARTER = 0.25f;
    private static final float HALF = 2 * QUARTER;
    private static final float THREE_QUARTERS = 3 * QUARTER;
    
    private final List<DockingPanel> analytics;
    private final List<DockingPanel> browsers;
    private final List<DockingPanel> editors;
    private final List<DockingPanel> stats;
    
    private javax.swing.JTabbedPane analyticTabs;
    private javax.swing.JTabbedPane browserTabs;
    private javax.swing.JTabbedPane editorTabs;
    private javax.swing.JTabbedPane statTabs;
    
}
