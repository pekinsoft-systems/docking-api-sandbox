/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   DockingApiSandbox.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking.trials;

import org.jdesktop.application.Application;
import org.jdesktop.docking.DockingView;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class DockingApiSandbox extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(DockingApiSandbox.class, args);
    }

    @Override
    protected void startup() {
        show(new DockingView((Application) this));
    }

}
