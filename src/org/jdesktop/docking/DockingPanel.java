/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   DockingApiSandbox
 *  Class      :   DockingPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 3, 2023
 *  Modified   :   Jan 3, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 3, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.docking;

import javax.swing.Icon;
import javax.swing.JPanel;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceMap;

/**
 * The `DockingPanel` abstract class is an extension of the {@link
 * javax.swing.JPanel JPanel} class that is setup for using within the {@link 
 * org.jdesktop.docking.DockingView DockingView} subclass of the {@link 
 * org.jdesktop.application.FrameView FrameView} class. Subclasses of 
 * `DockingPanel` must override the {@link #getTitle() getTitle()} and {@link 
 * #getIcon() getIcon()} methods. The {@link #isClosable() isClosable()}, {@link 
 * #isIconifiable() isIconifiable()}, and {@link #isMaximizable() isMaximizable()}
 * methods have default implementations that return `true`. The {@link 
 * #isExternalizable() isExternalizable()} and {@link #isMoveable() 
 * isMoveable()} methods have default implementations that return `false`. 
 * Subclasses only need to override these five (5) methods if they would like to
 * change the default behavior.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public abstract class DockingPanel extends JPanel {

    private static final long serialVersionUID = -2869602424743065673L;
    
    public static enum DockingLocations { BROWSERS, ANALYTICS, STATISTICS, EDITORS }
    
    protected DockingPanel (Application application) {
        context = application.getContext();
        resourceMap = context.getResourceMap(getClass());
        
        setTitle(resourceMap.getString(getClass().getSimpleName() + ".title"));
        setIcon(resourceMap.getIcon(getClass().getSimpleName() + ".icon"));
    }
    
    protected Application getApplication() {
        return context.getApplication();
    }
    
    protected Class getApplicationClass() {
        return context.getApplicationClass();
    }
    
    protected ApplicationContext getApplicationContext() {
        return context;
    }
    
    protected ResourceMap getResourceMap() {
        return resourceMap;
    }
    
    /**
     * Retrieves the title that should be displayed in the `DockingPanel`'s tab.
     * 
     * This value is set from the protected superclass constructor parameter
     * child, and is pulled from that child's `ResourceMap` setting.
     * 
     * @return the internationalized title for the `DockingPanel`'s tab
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * Provides a means by which the title may be changed. 
     * 
     * This method fires a {@link java.beans.PropertyChangeEvent PropertyChangeEvent} on the 
     * {@link org.jdesktop.application.ApplicationContext ApplicationContext}
     * against the property named "titleUpdated".
     * 
     * @param title the new title to use
     */
    public final void setTitle(String title) {
        String oldTitle = getTitle();
        this.title = title;
        
        context.firePropertyChange("titleUpdated", oldTitle, getTitle());
    }
    
    /**
     * Retrieves the icon that should be displayed on the `DockingPanel`'s tab.
     * 
     * This value is set from the protected superclass constructor parameter
     * `child`, and is pulled from that child's `ResourceMap` setting.
     * 
     * @return the (possibly localized) icon for the `DockingPanel`'s tab
     */
    public Icon getIcon() {
        return icon;
    }
    
    /**
     * Provides a means by which the {@link javax.swing.Icon Icon} may be changed.
     * This method fires a {@link java.beans.PropertyChangeEvent PropertyChangeEvent}
     * on the {@link org.jdesktop.application.ApplicationContext
     * ApplicationContext} against the property named "iconUpdated".
     * 
     * @param icon the new `Icon` to use
     */
    public final void setIcon(Icon icon) {
        Icon oldIcon = getIcon();
        this.icon = icon;
        
        context.firePropertyChange("iconUpdated", oldIcon, getIcon());
    }
    
    /**
     * Determines if the `DockingPanel` is allowed to be closed.
     * 
     * The default implementation returns `true`, allowing the `DockingPanel` to
     * be closed.
     * 
     * @return `true` to allow closing; `false` to prevent it
     */
    public boolean isClosable() {
        return true;
    }
    
    /**
     * Determines if the `DockingPanel` is allowed to be iconified.
     * 
     * The default implementation returns `true`, allowing the `DockingPanel` to
     * be iconified to the button bar.
     * 
     * @return `true` to allow iconifying; `false` to prevent it
     */
    public boolean isIconifiable() {
        return true;
    }
    
    /**
     * Determines if the `DockingPanel` is allowed to be maximized.
     * 
     * The default implementation returns `true`, allowing the `DockingPanel` to
     * be maximized to fill the entire client area of the window.
     * 
     * @return `true` to allow maximization; `false` to prevent it
     */
    public boolean isMaximizable() {
        return true;
    }
    
    /**
     * Determines if the `DockingPanel` is allowed to be externalized into its
     * own window.
     * 
     * The default implementation returns `false`, not allowing the `DockingPanel`
     * to be externalized. If a subclass wants to allow this functionality, it
     * will need to override this method.
     * 
     * @return `true` to allow externalization; `false` to prevent it (default
     * behavior)
     */
    public boolean isExternalizable() {
        return false;
    }
    
    /**
     * Determines if the `DockingPanel` is allowed to be dragged into another
     * docking location from its default. For example, dragging a browser dock
     * into the statistics docking location and vice versa.
     * 
     * The default implementation returns `false` to prevent the `DockingPanel`
     * from being moved into other locations. If a subclass want to allow this
     * functionality, it will need to override this method.
     * 
     * @return `true` to allow `DockingPanel` relocation; `false` to prevent it
     * (default behavior)
     */
    public boolean isMoveable() {
        return false;
    }
    
    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    
    private String title;
    private Icon icon;
    
    private transient DockingPanel child;
    
}
